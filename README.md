# Componente Práctico: Estadísticas Chat Muebles SAS

Esta solución contiene infraestructura como código y la implementación de una función lambda llamada _statistics-processor_ que recibe un evento desde S3 cuando se carga un archivo en formato .txt, se procesa y si existe coherencia entre los datos y el hash esperado, se almacena la información sobre una tabla de dynamoDB.

El patrón de diseño elegido por la naturaleza del problema y simplicidad es MVC, se cuenta con control de errores, librerías con clases para el manejo de S3 y DynamoDB, además se da respuesta a los eventos para la automatización de pruebas de integración (o de aceptación).

## Infraestructura

Sobre la raíz del proyecto, existe una carpeta llamada *infraestructure* que contiene el _template.yaml_ y _params.json_ de Cloudformation con la descripción de los recursos necesarios para la funcionalidad:

- Lambda function: Por defecto llamada _statistics-processor_.
- Bucket S3: Cuenta con el evento y permisos para invocar la función lambda una vez se cargue un objeto con extensión .txt. Por defecto llamado _muebles-sas-statistics_.
- DynamoDB: Tabla llamada por defecto _muebles-sas-statistics_ con llave de partición _timestamp_.

<img src="https://lh3.googleusercontent.com/drive-viewer/AJc5JmS2som-ea1SzEDDel6A802eeWI7ZS2Sh-a3oA8Hq3ZE9MWKHN0ZVl_93H0FTkD4n__mpA9ygpc=w986-h723">

## Despliegue

Puede desplegar la infraestructura usando aws-cli sobre su cuenta de AWS ejecutando los siguientes comandos (recuerde tener los permisos necesarios para usar cloudformation):

```sh
cd infrastructure
aws cloudformation create-stack --stack-name technical-test-diego-bernal --template-body file://template.yaml --parameters file://params.json --capabilities CAPABILITY_NAMED_IAM
cd ..
```
# Instalación

El proyecto cuenta con un archivo de configuración en la raíz del proyecto llamado _config.json_ que contiene dos parámetros:

- [ ] region: Región de AWS donde está desplegada la infraestructura
- [ ] statisticsTableName: Nombre de la tabla de dynamoDB donde se almacenarán las estadísticas.

Ejecución de pruebas unitarias:

```sh
npm test
npm run coverage
```

## Despliegue de Código

Si cuenta con una cli con credenciales de la cuenta de AWS, ejecute los siguientes comandos:

```sh
npm i
zip -r statisticsProcessor.zip node_modules
zip -g statisticsProcessor.zip app.js config.json package.json
zip -ur statisticsProcessor.zip src
aws lambda update-function-code --function-name statistics-processor-diegobernal --zip-file fileb://statisticsProcessor.zip
```
Ahora, puede cargar el archivo con las estadisticas en el bucket desplegado previamente, si los datos son correctos, verá la ejecución de la función lambda, la eliminación del archivo y el almacenamiento de los registros sobre la tabla de dynamoDB.

En caso de incompatibilidad con el hash, la función lambda responderá ante el evento con un objeto que contiene un status 400.

## Evidencia Funcionalidad

### Archivo con datos correctos

1. Estructura del archivo (debug/exampleFile.txt)
   
   ![alt text](https://gitlab.com/diegobernalm/statistics-processor-muebles-sas/-/raw/trunk/debug/exec1/1_archivo_prueba_correcto.png)

2. Carga de archivo sobre S3

    ![alt text](https://gitlab.com/diegobernalm/statistics-processor-muebles-sas/-/raw/trunk/debug/exec1/2_upload_archivo.png)

    ![alt text](https://gitlab.com/diegobernalm/statistics-processor-muebles-sas/-/raw/trunk/debug/exec1/2_carga_archivo_s3.png)

3. Ejecución de función lambda

    ![alt text](https://gitlab.com/diegobernalm/statistics-processor-muebles-sas/-/raw/trunk/debug/exec1/3_ejecucion_lambda.png)
    
4. Registro en DynamoDB

    ![alt text](https://gitlab.com/diegobernalm/statistics-processor-muebles-sas/-/raw/trunk/debug/exec1/4_AlmacenamientoDynamoDB.png)

5. Eliminación de archivo

    ![alt text](https://gitlab.com/diegobernalm/statistics-processor-muebles-sas/-/raw/trunk/debug/exec1/5_eliminacionArchivo.png)