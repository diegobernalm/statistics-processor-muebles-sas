/** Module to define application
 * @author Diego Alexánder Bernal Morales <diegobernalmo@gmail.com>
 * Iris Bank 2022
 */

const {
  healthController,
  statisticsController,
} = require('./src/controllers');

global.config = require('./config.json');

/**
 * Hanlder to lambda function
 * @param {object} event Event from S3
 */
async function handler(event) {
  if (event && event.type === 'health') return healthController();
  return statisticsController(event);
}

module.exports = {
  handler,
};
