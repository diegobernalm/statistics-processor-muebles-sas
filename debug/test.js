const healthEvent = require('./healthEvent.json');
const objectS3Event = require('./objectS3Event.json');
const app = require('../app');

(async () => {
  app.handler(healthEvent);
  app.handler(objectS3Event);
})();
