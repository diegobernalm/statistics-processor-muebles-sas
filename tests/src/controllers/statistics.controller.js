/* eslint-disable*/
const chai = require('chai');
const rewire = require('rewire');
const rewiremock = require('rewiremock').default;

const { expect } = chai;

const testCases = {
  1: (result) => {
    expect(result).is.a('object').that.have.a.property('status').is.equals(200);
    expect(result).is.a('object').that.have.a.property('message').is.a('string');
  },
  2: (result) => {
    expect(result).is.a('object').that.have.a.property('status').is.equals(400);
    expect(result).is.a('object').that.have.a.property('message').is.a('string');
  },
  3: (result) => {
    expect(result).is.a('object').that.have.a.property('status').is.equals(503);
  }
}

const mocks = {
  1: () => {
    modelMock = {
      statisticsModel: {
        processFile: async () => {
          return {
            hash: '2f941516446dce09bc2841da60bf811f',
            statistics: {
              totalContactoClientes:'250',
              motivoReclamo:'25',
              motivoGarantia:'10',
              motivoDuda:'100',
              motivoCompra:'100',
              motivoFelicitaciones:'7',
              motivoCambio:'8',
              hash:'2f941516446dce09bc2841da60bf811f'
            }
          }
        },
        saveStatistics: async () => { },
        deleteFile: async () => { }
      }
    }
    rewiremock('../models').with(modelMock);
  },
  2: () => {
    modelMock = {
      statisticsModel: {
        processFile: async () => {
          return {
            hash: '2f941516446dce09bc2841da60bf811f',
            statistics: {
              totalContactoClientes:'250',
              motivoReclamo:'25',
              motivoGarantia:'10',
              motivoDuda:'100',
              motivoCompra:'100',
              motivoFelicitaciones:'1',
              motivoCambio:'8',
              hash:'2f941516556dce09bc2841da60bf811f'
            }
          }
        },
        saveStatistics: async () => { },
        deleteFile: async () => { }
      }
    }
    rewiremock('../models').with(modelMock);
  },
  3: () => {
    modelMock = {
      statisticsModel: {
        processFile: async () => {
          throw new Error(`Error getting object`)
        },
        saveStatistics: async () => { },
        deleteFile: async () => { }
      }
    }
    rewiremock('../models').with(modelMock);
  }, 
}

async function statisticProcessorTestFuntion(done, testCase, event) {
  rewiremock.enable();
  mocks[testCase]();
  const statisticsController = rewire('../../../src/controllers/statistics.controller');
  const statisticProcessor = statisticsController.__get__('statisticProcessor');
  try {
    const result = await statisticProcessor(event);
    testCases[testCase](result);
    done();
  } catch (error) {
    done(error)
  }
  rewiremock.disable();
}


describe('src/controllers/statistics.controller.js', () => {
  describe('Function statisticProcessor', () => {
    it('The function successfully processes the file, stores the information and deletes the plain text file', (done) => {
      const event = { "Records": [{ "s3": { "bucket": { "name": "muebles-sas-statistics" }, "object": { "key": "exampleFile.txt" } } }] };
      statisticProcessorTestFuntion(done, 1, event);
    });
    it('The plain text file is processed but is corrupt', (done) => {
      const event = { "Records": [{ "s3": { "bucket": { "name": "muebles-sas-statistics" }, "object": { "key": "exampleFile.txt" } } }] };
      statisticProcessorTestFuntion(done, 2, event);
    });
    it('An error occurred processing the file', (done) => {
      const event = { "Records": [{ "s3": { "bucket": { "name": "muebles-sas-statistics" }, "object": { "key": "exampleFile.txt" } } }] };
      statisticProcessorTestFuntion(done, 3, event);
    });
  });
});
