/* eslint-disable*/
const chai = require('chai');
const rewire = require('rewire');

const { expect } = chai;

async function healthTestFuntion(done) {
  const healthController = rewire('../../../src/controllers/health.controller');
  const health = healthController.__get__('health');
  try {
    const result = await health();
    expect(result).to.be.a('object').that.have.property('message').is.equals('I am alive');
    expect(result).to.be.a('object').that.have.property('status').is.equals(200);
    done();
  } catch (error) {
    done(error)
  }
}

describe('src/controllers/health.controller.js', () => {
  describe('Function status', () => {
    it('The function returns the expected data', (done) => {
      healthTestFuntion(done);
    });
  });
});
