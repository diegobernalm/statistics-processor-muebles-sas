/* eslint-disable*/
const chai = require('chai');
const rewire = require('rewire');
const rewiremock = require('rewiremock').default;

const { expect } = chai;

const testCases = {
  1: (result) => {
    expect(result).is.a('object').that.have.a.property('hash').is.equals('2f941516446dce09bc2841da60bf811f');
    expect(result).is.a('object').that.have.a.property('statistics').is.a('object');
    expect(result.statistics).have.a.property('hash').is.equals(result.hash)
  },
  2: (result) => {
    expect(result).is.a('object').that.have.a.property('hash').is.a('string');
    expect(result).is.a('object').that.have.a.property('statistics').is.a('object');
    expect(result.statistics).have.a.property('hash').is.not.equals(result.hash)
  },
}

const s3Libmocks = {
  1: () => {
    libMock = {
      S3Lib: class {
        constructor() { }
        async getObjectData() {
          return 'totalContactoClientes=250\nmotivoReclamo=25\nmotivoGarantia=10\nmotivoDuda=100\nmotivoCompra=100\nmotivoFelicitaciones=7\nmotivoCambio=8\nhash=2f941516446dce09bc2841da60bf811f'
        }
      }
    }
    rewiremock('../libs').with(libMock);
  },
  2: () => {
    libMock = {
      S3Lib: class {
        constructor() { }
        async getObjectData() {
          return 'totalContactoClientes=1\nmotivoReclamo=25\nmotivoGarantia=10\nmotivoDuda=100\nmotivoCompra=100\nmotivoFelicitaciones=7\nmotivoCambio=8\nhash=2f941516446dce09bc2841da60bf811f'
        }
      }
    }
    rewiremock('../libs').with(libMock);
  },
  3: () => {
    libMock = {
      S3Lib: class {
        constructor() { }
        async getObjectData() {
          throw new Error('Object unknown');
        }
      }
    }
    rewiremock('../libs').with(libMock);
  },
}

async function processFileTestFuntion(done, testCase) {
  rewiremock.enable();
  s3Libmocks[testCase]();
  const statisticsModel = rewire('../../../src/models/statistics.model');
  const processFile = statisticsModel.__get__('processFile');
  try {
    const result = await processFile('testFile', 'testObject');
    testCases[testCase](result);
    done();
  } catch (error) {
    done(error);
  }
  rewiremock.disable();
}


async function saveStatisticsTestFuntion(done) {
  rewiremock.enable();
  global.config = { statisticsTableName: 'myTable' }
  libMock = {
    DynamoDBLib: class {
      constructor() { }
      async putItem() { }
    }
  }
  rewiremock('../libs').with(libMock);
  const statisticsModel = rewire('../../../src/models/statistics.model');
  const saveStatistics = statisticsModel.__get__('saveStatistics');
  try {
    await saveStatistics({});
    done();
  } catch (error) {
    done(error);
  }
  rewiremock.disable();
}

async function deleteFileTestFuntion(done) {
  rewiremock.enable();
  libMock = {
    S3Lib: class {
      constructor() { }
      async getObjectData() {
        return 'totalContactoClientes=250\nmotivoReclamo=25\nmotivoGarantia=10\nmotivoDuda=100\nmotivoCompra=100\nmotivoFelicitaciones=7\nmotivoCambio=8\nhash=2f941516446dce09bc2841da60bf811f'
      }
      async deleteObject() { }
    },
  }
  rewiremock('../libs').with(libMock);
  const statisticsModel = rewire('../../../src/models/statistics.model');
  const processFile = statisticsModel.__get__('processFile');
  await processFile();
  const deleteFile = statisticsModel.__get__('deleteFile');
  try {
    await deleteFile({});
    done();
  } catch (error) {
    done(error);
  }
  rewiremock.disable();
}


describe('src/models/statistics.model.js', () => {
  describe('Function processFile', () => {
    it('The function successfully processes the file and return hash and statistics', (done) => {
      processFileTestFuntion(done, 1);
    });
    it('The function successfully processes the file and return hash and statitistics with different hash', (done) => {
      processFileTestFuntion(done, 2);
    });
  });
  describe('Function saveStatistics', () => {
    it('Saving statistics succesfully', (done) => {
      saveStatisticsTestFuntion(done);
    });
  });
  describe('Function deleteFile', () => {
    it('Deletting file succesfully', (done) => {
      deleteFileTestFuntion(done);
    });
  });
});
