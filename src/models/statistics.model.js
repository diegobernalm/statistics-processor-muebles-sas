/** Model to statistics processor
 * @author Diego Alexánder Bernal Morales <diegobernalmo@gmail.com>
 * Iris Bank 2022
 * @requires aws-sdk
 * @requires crypto
 * @requires ../libs
 */

const crypto = require('crypto');

const { DynamoDBLib, S3Lib } = require('../libs');

let s3Lib;

async function processFile(bucket, object) {
  const stat = {};
  s3Lib = new S3Lib(bucket);
  const content = (await s3Lib.getObjectData(object)).split('\n');
  content.forEach((item) => {
    const field = item.split('=');
    stat[field[0]] = field[1];
  });
  const base = `${stat.totalContactoClientes}~${stat.motivoReclamo}~${stat.motivoGarantia}~${stat.motivoDuda}~${stat.motivoCompra}~${stat.motivoFelicitaciones}~${stat.motivoCambio}`
  const hash = crypto.createHash('md5').update(base).digest('hex');
  return {
    hash,
    statistics: stat,
  };
}

async function saveStatistics(data) {
  const dynamodbLib = new DynamoDBLib(global.config.statisticsTableName);
  data.timestamp = Date.now().toString();
  await dynamodbLib.putItem(data);
}

async function deleteFile(object) {
  await s3Lib.deleteObject(object);
}

module.exports = {
  processFile,
  deleteFile,
  saveStatistics,
};
