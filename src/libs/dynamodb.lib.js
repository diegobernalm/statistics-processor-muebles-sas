const AWS = require('aws-sdk');

class DynamoDBLib {
  constructor(tableName) {
    AWS.config.update({ region: global.config.region });
    this.tableName = tableName;
    this.clientDy = new AWS.DynamoDB.DocumentClient({ apiVersion: '2012-08-10' });
  }

  async putItem(data) {
    try {
      await this.clientDy.put({
        TableName: this.tableName,
        Item: data,
      }).promise();
    } catch (error) {
      throw new Error(`[DynamoDBLib][putItem] put item on dynamoDB: ${JSON.stringify(data)}\nDetail: ${error.stack}`);
    }
  }
}

module.exports = DynamoDBLib;
