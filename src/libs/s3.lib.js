const AWS = require('aws-sdk');

const s3 = new AWS.S3();

class S3Lib {
  constructor(bucketname) {
    this.bucketname = bucketname;
  }

  async getObjectData(object) {
    try {
      const data = await s3.getObject({
        Bucket: this.bucketname,
        Key: object,
      }).promise();
      return data.Body.toString();
    } catch (error) {
      throw new Error(`[s3.lib.js][getObjectData] failed getting object ${this.bucketname}/${object}\nDetail: ${error.stack}`);
    }
  }

  async deleteObject(object) {
    try {
      await s3.deleteObject({
        Bucket: this.bucketname,
        Key: object,
      }).promise();
    } catch (error) {
      throw new Error(`[s3.lib.js][deleteObject] failed deleting object ${this.bucketname}/${object}\nDetail: ${error.stack}`);
    }
  }
}

module.exports = S3Lib;
