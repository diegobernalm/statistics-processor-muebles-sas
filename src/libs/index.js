module.exports = {
  DynamoDBLib: require('./dynamodb.lib'),
  S3Lib: require('./s3.lib'),
};
