/** Controller to statistics processor
 * @author Diego Alexánder Bernal Morales <diegobernalmo@gmail.com>
 * Iris Bank 2022
 */

const { statisticsModel } = require('../models');

async function statisticProcessor(event) {
  try {
    const bucket = event.Records[0].s3.bucket.name;
    const object = event.Records[0].s3.object.key;
    const {
      statistics,
      hash,
    } = await statisticsModel.processFile(bucket, object);
    if (hash === statistics.hash) {
      await statisticsModel.saveStatistics(statistics);
      await statisticsModel.deleteFile(object);
      return {
        status: 200,
        message: `The ${bucket}/${object} object was processed sucessfully`,
      };
    }
    return {
      status: 400,
      message: `The ${bucket}/${object} object is corrupt`,
    };
  } catch (error) {
    console.error(`Failed proccesing object.\nEvent: ${event}`);
    console.error(`Error detail: ${error.stack}`);
    return {
      status: 503,
      message: 'Failed processing object. View logs to more details.',
    };
  }
}

module.exports = statisticProcessor;
