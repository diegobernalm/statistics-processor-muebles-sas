module.exports = {
  healthController: require('./health.controller'),
  statisticsController: require('./statistics.controller'),
};
