/** Controller to health event
 * @author Diego Alexánder Bernal Morales <diegobernalmo@gmail.com>
 * Iris Bank 2022
 */

async function health() {
  return {
    status: 200,
    message: 'I am alive',
    timestamp: Date.now().toString(),
  };
}

module.exports = health;
